from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in asv/__init__.py
from asv import __version__ as version

setup(
	name='asv',
	version=version,
	description='Atelier du Soleil et du Vent',
	author='Guillaume AUGAIS',
	author_email='atelierdusoleiletduvent@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
