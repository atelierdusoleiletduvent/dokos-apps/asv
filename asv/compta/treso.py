import frappe

DEPENSES = 'Dépenses'
RECETTES = 'Recettes'

def categorized_bank_entries(start_date_str, end_date_str):
    summary = {}
    
    categorized_journal_entries(start_date_str, end_date_str, summary)
    categorized_payment_entries(start_date_str, end_date_str, summary)

    printSummary(summary)


def categorized_journal_entries(start_date_str, end_date_str, summary):
    filters = [
        ['posting_date', 'between', [start_date_str, end_date_str]],
        ['docstatus', '=', '1'],
    ]
    xs = frappe.db.get_all('Journal Entry', filters, ['name', 'posting_date'])    
    for x in xs:
        categorize_journal_entry(x.name, summary)

def categorize_journal_entry(je_name, summary):
    je = frappe.get_doc('Journal Entry', je_name)

    print(f"---------------------")
    print(f"{je.name}")

    categorized = False

    # skip if no accounts starting with 5xx
    skip = True
    for acc in je.accounts:
        if acc.account_type == 'Bank':
            skip = False 
    if (skip):
        print(f"skipping Journal Entry {je_name}")
        return
    
    # TODO eventually: try implement simple journal entry (if not too complex)
    
    if not categorized:
        appendAmountToSummary(summary, "UNKNOWN", f"[{je.name}] {je.title}", je.total_amount)
        print(f' *** à gérer {je.name} | {je.title}')


def categorized_payment_entries(start_date_str, end_date_str, summary):
    filters = [
        ['posting_date', 'between', [start_date_str, end_date_str]],
        ['status', 'in', ['Reconciled', 'Unreconciled']],
        ['payment_type', 'in', ['Receive', 'Pay']]
    ]
    xs = frappe.db.get_all('Payment Entry', filters, ['name', 'posting_date', 'status', 'paid_amount'])

    for x in xs:
        pe = frappe.get_doc('Payment Entry', x.name)

        print(f"---------------------")
        print(f"{pe.name}")
        
        for ref in pe.references:
            refdt = ref.reference_doctype
            
            alloc_amount = ref.allocated_amount
            tot_amount = ref.total_amount

            if (refdt == 'Sales Invoice'):
                sinv = frappe.get_doc('Sales Invoice', ref.reference_name)
                print("")
                print(f"\t Recette : \t+{ref.allocated_amount}€ \t{ref.reference_name} \t{sinv.customer_name}")
                
                # make sure it's not a partial payment (to be handled manually)
                items_amount = 0
                for item in sinv.items:
                    items_amount += item.amount

                if (alloc_amount == items_amount):
                    for item in sinv.items:
                        (dr, cat) = item_to_cats(refdt, item.item_code, item.item_group, ref_name=ref.reference_name)
                        print(f"\t - {item.item_code} ({item.item_group}) : {item.amount}")
                        appendAmountToSummary(summary, dr, cat, item.amount)
                else:
                    appendAmountToSummary(summary, RECETTES, f"{ref.reference_name} via {x.name}", alloc_amount)

            elif (refdt == 'Purchase Invoice'):
                
                pinv = frappe.get_doc('Purchase Invoice', ref.reference_name)
                print("")
                print(f"\t Dépense : \t+{ref.allocated_amount}€ \t{ref.reference_name}")

                # make sure it's not a partial payment (to be handled manually)
                items_amount = 0
                for item in pinv.items:
                    items_amount += item.amount

                if (alloc_amount == items_amount):
                    for item in pinv.items:
                        (dr, cat) = item_to_cats(refdt, item.item_code, item.item_group)
                        items_amount += item.amount
                        print(f"\t - {item.item_code} ({item.item_group}) : {item.amount}")
                        appendAmountToSummary(summary, dr, cat, item.amount)
                else:
                    appendAmountToSummary(summary, DEPENSES, ref.reference_name, alloc_amount)

            elif (refdt == 'Expense Claim'):
                exp = frappe.get_doc('Expense Claim', ref.reference_name)

                print("")
                print(f"\t Note de frais : \t+{exp.total_amount_reimbursed}€ \t{exp.name}")

                for xp in exp.expenses:
                    xptpe = xp.expense_type
                    (dr, cat) = item_to_cats(refdt, xptpe, xptpe)
                    print(f"\t - {xptpe} ({xp.description}) : {xp.amount}")
                    appendAmountToSummary(summary, dr, cat, xp.amount)
            
            elif (refdt == 'Journal Entry'):
                je = frappe.get_doc('Journal Entry', ref.reference_name)

                appendAmountToSummary(summary, "UNKNOWN", f"[{je.name} via {pe.name}] {je.title} / {pe.reference_no}", je.total_amount)
                print(f' *** à gérer {je.name} | {je.title}')
            else:
                errmsg = f' *** Unexpected {ref.reference_doctype}'
                print(errmsg)
                raise Exception(errmsg) 
                

def appendAmountToSummary(summary, dr, cat, amount):
    if dr not in summary:
        summary[dr] = {}
    if cat not in summary[dr]:
        summary[dr][cat] = []
    
    summary[dr][cat].append(amount)

def printSummary(summary):
    print("============")
    print("")
    for dr in summary.keys():
        print("-----------")
        print(dr)
        for cat in summary[dr].keys():
            xs = summary[dr][cat]
            tot = "%.2f" % sum(xs)
            print("")
            print(f"\t{cat}\t\t {tot}")
            print(f"\t{xs}")
            


def item_to_cats(reference_doctype, item_code, item_group, **kwargs):
    
    # recettes
    RECETTES_ADH        = (RECETTES, 'Adhésions')
    RECETTES_STAGES     = (RECETTES, 'Stage / Initiation / Formation')
    RECETTES_RESA_PRIVES_ATELIERS = (RECETTES, 'Résa privés des ateliers')
    RECETTES_ANIMATION = (RECETTES, 'Animation')
    RECETTES_ATELIERS_PARTAGES_PROS = (RECETTES, 'Ateliers partagés pros')
    RECETTES_AUTRES = (RECETTES, "Autres")
    
    # depenses
    DEPENSES_REMUNERATION_FORMATEURS_ENCADRANTS = (DEPENSES, 'Rémunération Formateurs / Encadrants')
    
    #   petit équipement
    DEPENSES_FOURNITURE_PETIT_EQUIPEMENT_ATELIER = (DEPENSES, 'Petit équipement atelier')

    #   divers
    DEPENSES_FOURNITURES_DIVERSES = (DEPENSES, 'Fournitures diverses')
    DEPENSES_FRAIS_DEPLACEMENT = (DEPENSES, 'Frais de déplacements')
    DEPENSES_POSTE_TELECOM = (DEPENSES, "Poste et télécommunication")
    DEPENSES_LICENCES_LOGICIELS_NOM_DOMAINE = (DEPENSES, 'Licences / logiciels / nom de domaine')
    DEPENSES_ABONNEMENT_COTISATION = (DEPENSES, 'Abonnements, cotisations')
    DEPENSES_FRAIS_BANCAIRES = (DEPENSES, 'Frais bancaires')
    DEPENSES_AUTRES = (DEPENSES, "Autres")
    DEPENSES_SERVEUR_INTERNET = (DEPENSES, "Serveur internet")

    if False:
        print("Never happends !")

    # note de frais
    elif reference_doctype == 'Expense Claim':
        if item_code == 'Fournitures administratives' or item_code == 'Fournitures Entretien & Petit équipement':
            return DEPENSES_FOURNITURES_DIVERSES
        elif item_code == 'Alimentation':
            return DEPENSES_FOURNITURES_DIVERSES
        elif item_code == 'Cotisation / adhésion':
            return DEPENSES_ABONNEMENT_COTISATION
        elif item_code == 'Déplacement':
            return DEPENSES_FRAIS_DEPLACEMENT
        elif item_code == 'Licences / logiciels / nom de domaine':
            return DEPENSES_LICENCES_LOGICIELS_NOM_DOMAINE

    # recettes
    elif item_code in ['ADH-2022']:
        return RECETTES_ADH
    elif item_group == 'Stages' and reference_doctype == 'Sales Invoice':
        return RECETTES_STAGES
    elif item_code == 'ACOMPTE-BON-CADEAU-2022':
        return RECETTES_STAGES
    elif item_code == 'ACCES-ATELIER-3000-NON-PRO-JOURNEE':
        return RECETTES_ATELIERS_PARTAGES_PROS
    elif item_code == 'RESA-ATELIER-PRO' and reference_doctype == 'Sales Invoice':
        return RECETTES_RESA_PRIVES_ATELIERS
    elif item_code == 'ANIM-FORMATION-CHANTIER-PARTICIPATIF' and reference_doctype == 'Sales Invoice':
        return RECETTES_ANIMATION
    elif item_code == 'FRAIS-GESTION' and reference_doctype == 'Sales Invoice':
        return RECETTES_AUTRES
    
    # depenses
    elif item_code == 'PETIT-EQPMT-CONSOMMABLE-METAL':
        return DEPENSES_FOURNITURE_PETIT_EQUIPEMENT_ATELIER
    elif item_code == 'Petit équipement - Aménagement Ateliers':
        return DEPENSES_FOURNITURE_PETIT_EQUIPEMENT_ATELIER
    elif item_group == 'Stages' and reference_doctype == 'Purchase Invoice':
        return DEPENSES_REMUNERATION_FORMATEURS_ENCADRANTS
    elif item_code == 'ORGA-ACTIVITE-ATELIERS-ET-STAGES' and reference_doctype == 'Purchase Invoice':
        return DEPENSES_REMUNERATION_FORMATEURS_ENCADRANTS
    elif item_code == 'Fournitures Entretien & Petit équipement':
        return DEPENSES_FOURNITURES_DIVERSES
    elif item_code in ['BQ-ABO-COOP', 'BQ-FRAIS-VIRMT-SEPA', 'STRIPE-FEES']:
        return DEPENSES_FRAIS_BANCAIRES
    elif item_code == 'FRAIS-GESTION' and reference_doctype == 'Purchase Invoice':
        return DEPENSES_AUTRES

    # autres / divers
    elif item_code == 'LOC-SERVEUR-DEDIBOX':
        return DEPENSES_SERVEUR_INTERNET
    elif item_code == 'FREE-ABO' or item_code == 'FREE-COMM':
        return DEPENSES_POSTE_TELECOM
    else:
        ref_name = kwargs['ref_name'] if 'ref_name' in kwargs else None
        print(f"(à catégoriser) UNKNOWN item for {reference_doctype} / {item_code}")
        return (f"UNKNOWN", f"{item_code} via {ref_name}" if ref_name else item_code)