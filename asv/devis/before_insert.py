import frappe
from asv.adhesion import adherents

def insert_adh_if_needed(doc, method):
    
    # CURRENT_ADH_ITEM = "ADH-2023"

    if doc.order_type == 'Shopping Cart':
        dialog_msg = "Le stage a bien été ajouté à votre 'Panier'.\n Une adhésion 2023 à 10€ a également été ajoutée (requise pour participer).\nContactez-nous si vous êtes déjà adhérent·e"
        dialog_title = 'Adhésion 2023'
    else:
        dialog_msg = "Adhésion 2023 ajoutée : obligatoire pour un 'Stage' et non détectée."
        dialog_title = 'Ajout automatique'

    if doc.quotation_to == "Customer":
        cname = doc.party_name
        email = doc.contact_email
    

    # year = CURRENT_ADH_ITEM.split("-")[1]
    year = frappe.utils.today().split("-")[0]

    adh_status, adh_debug = adherents.statut_adhesion_for_adherent(year, email, cname)

    i = frappe.get_doc("Item", "ADH-2023")

    # for imqi in doc.items:
    #     frappe.msgprint(f"in memory : {imqi.as_dict()}")

    # current_items = []
    # for di in doc.items:
    #     # if frappe.db.exists("Quotation Item", "jane@example.org", cache=True)
    #     ci = frappe.get_doc('Quotation Item', di.name)
    #     # except DoesNotExistError as e:
    #     current_items.append(ci)

    # for ci in current_items:
    #     frappe.msgprint(ci.item_code)



    if adh_status == "no":
        
        # return early if quotation already contains ADH-2023
        should_add_adhesion = True
        for ci in doc.items:
            if ci.item_code == "ADH-2023":
                should_add_adhesion = False
                # print("returning early (nothing after this)")
                # frappe.msgprint("Adhésion déjà prise", title = "Adhésion ok")
                # return

        # otherwise
        # if some item has group 'Stages'
        # then add item 'ADH-2023' to quotation
        containsSomeStagesItem = False
        for ci in doc.items:
            if ci.item_group == "Stages":
                containsSomeStagesItem = True
        
        if containsSomeStagesItem and should_add_adhesion:
            doc.append("items", {
                'item_code' : i.item_code,
                'item_name' : i.item_name,
                'description' : i.description,
                'qty' : 1,
                'uom' : i.stock_uom,
                'conversion_factor' : 1,
                'rate' : i.standard_rate,
                'amount': i.standard_rate,
            })
            doc.calculate_taxes_and_totals()
            frappe.msgprint(
                msg=dialog_msg,
                title=dialog_title,
            )
    