import frappe

def notify_asv_after_submit(doc, method):

    if doc.order_type != 'Shopping Cart':
        return

    if doc.status != 'Open':
        return
    
    recipients = [
        'atelierdusoleiletduvent+devis@gmail.com'
    ]

    contact = frappe.get_doc('Contact', doc.contact_person)

    frappe.sendmail(
        recipients=recipients,
        subject=f'Demande de Devis {doc.name}',
        content=f"""
Une demande de devis vient d'être réalisée sur le site internet : 

- client : [{contact.first_name} {contact.last_name}](https://www.atelierdusoleiletduvent.org/app/contact/{doc.contact_person})
- devis : [{doc.name}](https://www.atelierdusoleiletduvent.org/app/quotation/{doc.name})

Merci de :

- vérifier la disponibilité du ou des articles / journées de stage
- réaliser une commande client par article / stage (depuis la [page du devis](https://www.atelierdusoleiletduvent.org/app/quotation/{doc.name}))
- pour chaque commande client, de générer : une facture d'acompte, puis d'envoyer le lien de paiement associé
""",
    as_markdown=True,
    header=f'Devis {doc.name} : accusé de réception et prochaines étapes.'
)

def notify_user_after_submit(doc, method):

    if doc.order_type != 'Shopping Cart':
        return

    if doc.status != 'Open':
        return
    
    recipients = [
        doc.contact_email
    ]

    contact = frappe.get_doc('Contact', doc.contact_person)

    frappe.sendmail(
        recipients=recipients,
        subject=f'Demande de Devis {doc.name}',
        content=f"""
Bonjour {contact.first_name},

Nous accusons réception de votre **demande de devis**, réalisée depuis notre site internet.


Voici les prochaines étapes.


Dans les prochains jours :

- nous **vérifions les disponibilités** de notre côté
- nous vous **envoyons un lien de paiement sécurisé**, pour régler par carte bancaire (ce qui finalisera votre inscription)


Ensuite :

- vous êtes notifiés lors de la réception de votre paiement


Bien que nous préférons un règlement par carte bancaire, vous pouvez également régler par chèque ou virement.

Merci de répondre à ce mail en nous le précisant le cas échéant.

_À noter que votre inscription ne sera validée qu'à réception du paiement._


Si votre demande concerne un évènement qui se déroule dans les prochains jours,  
Merci de nous contacter au [09 50 86 32 89](tel://0950863289) et de laisser un message si nécessaire.


Bien cordialement,

""",
        as_markdown=True,
        header=f'Devis {doc.name} : accusé de réception et prochaines étapes.'
    )

def notify_by_email_www(doc, method):

    if doc.order_type != 'Shopping Cart':
        return

    if doc.status != 'Draft':
        return
    
    recipients = [
        'atelierdusoleiletduvent+devis@gmail.com'
    ]

    articles = [ f"{i.item_code}\t\t{i.qty}" for i in doc.items]
    articles_out = "\n- ".join(articles)

    frappe.sendmail(
        recipients=recipients,
        subject=f'Devis {doc.name} / {doc.customer_name} (brouillon)',
        content=f"""
Ce message est généré à l'attention de l'organisateur des stages.

Une personne vient d'ajout le ou les articles suivant à son panier.

A ce stade, **le panier n'a pas encore été validé**.

Peut-être que cette personne a **besoin d'aide** pour soumettre le devis et ainsi passer commande ?

**Merci de la recontacter si aucune autre notification ne suit** dans les prochaines minutes, ou prochaines heures...

Lien : <a href="https://www.atelierdusoleiletduvent.org/app/quotation/{doc.name}">{doc.name}</a>

Statut : **Brouillon** (ajouté au panier mais non validé !)  

Client : <a href="https://www.atelierdusoleiletduvent.org/app/customer/{doc.party_name}">{doc.party_name}</a>

**Articles :**  

{articles_out}
        """,
        as_markdown=True,
        header=f'1er article : {doc.items[0].item_code}'
    )