import frappe
from asv.custom.utils import rot5, rot13
import locale
from datetime import datetime
import re
from asv.stage.semantic import structured_data_as_product

def strip_ql_editor_div(html):
    start = """<div class="ql-editor read-mode">"""
    end = """</div>"""
    out = html
    if html.startswith(start) and html.endswith(end):
        out = html[len(start):len(html)-len(end)]
    return out

def strip_first_p(html):
    start = """<p>"""
    end = """</p>"""
    out = html
    if html.startswith(start) and html.endswith(end):
        out = html[len(start):len(html)-len(end)]
    return out

def fetch_related_stage(item_name):
    stage_names = frappe.db.get_list('Stage', filters={'reference_article': item_name}, pluck='name')
    
    stage = None
    
    if len(stage_names) == 1:
        stage_name = stage_names[0]
        stage = frappe.get_doc('Stage', stage_name)
    elif len(stage_names) > 1:
        stage = None
        debug = " / ".join(stage_names)
        frappe.msgprint(f"ATTENTION: 1 stage pour 1 article attendu mais {len(stage_names)} stages trouvés : {debug}")
    else:
        stage = None
    
    return stage

def update_info_with_stage_details(item, method=None):
    stage = fetch_related_stage(item.name)
    
    if stage:
        item_update_website_content(item, stage)
        item_update_web_long_description(item, stage)
        item_update_image(item, stage)
        print("'Article' mis à jour avec les infos du 'Stage' associé")
        frappe.msgprint("'Article' mis à jour avec les infos du 'Stage' associé")

def parse_regular_date(regular_date_pretty):
    d = None
    try:
        d = datetime.strptime(regular_date_pretty, '%d %B %Y')
    except ValueError as e:
        frappe.msgprint(f"Erreur : format de date invalide '{regular_date_pretty}'")
    return d

def parse_regular_date_with_weekday(regular_date_with_weekday_pretty):
    d = None
    try:
        d = datetime.strptime(regular_date_with_weekday_pretty, '%A %d %B %Y')
    except ValueError as e:
        frappe.msgprint(f"Erreur : format de date invalide '{regular_date_with_weekday_pretty}'")
    return d

def parse_weekend_date(weekend_date_pretty):
    last_day = None
    m = re.match(r'Week-end\sdu\s([^-]+)-([^\s]+)\s([^\s]+)\s([0-9]+)', weekend_date_pretty)
    if len(m.groups()) == 4:
        first_day_as_list = m.group(1,3,4)
        last_day_as_list = m.group(2,3,4)
        
        first_day_pretty = " ".join(first_day_as_list) # exemple : '10 Juillet 2022'
        last_day_pretty = " ".join(last_day_as_list) # exemple : '11 Juillet 2022'
        
        first_day = parse_regular_date(first_day_pretty)
        last_day = parse_regular_date(last_day_pretty)
    else:
        frappe.msgprint(f"Erreur : format de date invalide '{weekend_date_pretty}'")
    return first_day, last_day

def parse_start_and_end_date(date_pretty):
    old_locale = locale.getlocale()
    locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF-8'))

    start = None
    end = None

    if date_pretty.startswith("Week-end du "):
        start, end = parse_weekend_date(date_pretty)
    else:
        start = parse_regular_date_with_weekday(date_pretty)
        end = start

    locale.setlocale(locale.LC_ALL, old_locale)
    return start, end

def parse_date(date_pretty):
    start, end = parse_start_and_end_date(date_pretty)
    return end

def order_pretty_dates(dates_pretty):
    """Tri une liste de date au format 'Dimanche 6 Mars 2016' et supprime les dates invalides (fails silently)"""
    old_locale = locale.getlocale()
    # print(old_locale)
    locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF-8'))

    ds_tuples = []

    for d_pretty in dates_pretty:
        d = parse_date(d_pretty)
        ds_tuples.append((d, d_pretty))

    locale.setlocale(locale.LC_ALL, old_locale)
    ordered_by_asc = sorted(ds_tuples, key=lambda tup: tup[0])

    return [o[1] for o in ordered_by_asc]

def get_date_variants_with_meta_for_model(item_model):
    date_variants_meta = None
    item = item_model

    if item.has_variants and 'Item Attribute' in item.variant_based_on:
        attrs = item.attributes

        # Date
        date_attr = next((a for a in item.attributes if a.attribute == 'Date'), None)

        if date_attr:
            all_date_variants = frappe.db.get_list('Item Variant Attribute', 
                filters={
                    'variant_of': item.name,
                    'attribute': 'Date'
                }, 
                fields=['name', 'attribute', 'attribute_value', 'variant_of', 'parenttype', 'parent']
            )
            
            date_variants_meta = [ 
                {'item_code': dv.parent, 'date_attribute': dv.attribute_value, 'date': parse_date(dv.attribute_value) }
                for dv in all_date_variants 
                if (
                        dv.attribute == 'Date' and dv.attribute_value is not None and 
                        dv.parenttype == 'Item' and frappe.get_value('Item', dv.parent, 'disabled') == 0
                    )
            ]
    
    return date_variants_meta

def get_date_variants_for_model(item_model):
    date_variants = None
    item = item_model

    if item.has_variants and 'Item Attribute' in item.variant_based_on:
        attrs = item.attributes

        # Date
        date_attr = next((a for a in item.attributes if a.attribute == 'Date'), None)

        if date_attr:
            all_date_variants = frappe.db.get_list('Item Variant Attribute', 
                filters={
                    'variant_of': item.name,
                    'attribute': 'Date'
                }, 
                fields=['name', 'attribute', 'attribute_value', 'variant_of', 'parenttype', 'parent']
            )
            date_variants = [ dv for dv in all_date_variants if dv.parenttype == 'Item' and frappe.get_value('Item', dv.parent, 'disabled') == 0 ]
    
    return date_variants

def get_bon_cadeau_variants_for(item_model):
    bon_cadeau_variants = None
    item = item_model

    if item.has_variants and 'Item Attribute' in item.variant_based_on:
        
        bon_cadeau_attr = next((a for a in item.attributes if a.attribute == 'Bon cadeau'), None)

        # Bon cadeau
        if bon_cadeau_attr:
            bon_cadeau_variants = frappe.db.get_list('Item Variant Attribute', 
                filters={
                    'variant_of': item.name,
                    'attribute': 'Bon cadeau'
                }, 
                fields=['name', 'attribute', 'attribute_value', 'variant_of']
            )
    
    return bon_cadeau_variants
        

def make_context_for_rendering(item, stage):
    date_variants = get_date_variants_for_model(item)
    bon_cadeau_variants = get_bon_cadeau_variants_for(item)

    pretty_dates = [ variant.attribute_value for variant in date_variants if variant.attribute == 'Date' and variant.attribute_value is not None] if date_variants else []
    ordered_pretty_dates = order_pretty_dates(pretty_dates)
    context = {
        'description': stage.resume if stage and stage.resume else None, 
        'dates': ordered_pretty_dates,
        'item_code': item.name,
        'bons_cadeaux': bon_cadeau_variants,
        'structured_data_product': structured_data_as_product(item, stage)
        }
    return context

def render_web_long_description(item, stage):
    context = make_context_for_rendering(item, stage)
    return frappe.render_template('stage/web_long_description_from_stage.html', context)


def item_update_web_long_description(item, stage):
    desc = render_web_long_description(item, stage)
    frappe.db.set_value("Item", item.name, "web_long_description", desc, update_modified=False)

def item_update_image(item, stage):
    if stage.image_principale:
        frappe.db.set_value("Item", item.name, "website_image", stage.image_principale, update_modified=False)
        frappe.db.set_value("Item", item.name, "image", stage.image_principale, update_modified=False)

def item_update_website_content(item, stage):
    encadrant_principal = stage.encadrants_potentiels[0] if len(stage.encadrants_potentiels) > 0 else {}
    organisation = strip_first_p(strip_ql_editor_div(encadrant_principal.organisation)) if encadrant_principal.organisation else None
    # frappe.msgprint(f"organisation : {organisation} from {encadrant_principal.as_dict()}")

    stage.hebergement   = strip_ql_editor_div(stage.hebergement)    if stage.hebergement else None
    stage.repas         = strip_ql_editor_div(stage.repas)          if stage.repas else None
    stage.presentation  = strip_ql_editor_div(stage.presentation)   if stage.presentation else None
    stage.objectifs     = strip_ql_editor_div(stage.objectifs)      if stage.objectifs else None
    stage.programme     = strip_ql_editor_div(stage.programme)      if stage.programme else None
    stage.prerequis     = strip_ql_editor_div(stage.prerequis)      if stage.prerequis else None
    stage.details       = strip_ql_editor_div(stage.details)        if stage.details else None

    context = {
        'stage': (stage if stage else {}),
        'encadrant_principal': encadrant_principal,
        'organisation': organisation
        }
            
    website_content_html = frappe.render_template('stage/website_content_from_stage.html', context)
    frappe.db.set_value("Item", item.name, "website_content", website_content_html, update_modified=False)
    
