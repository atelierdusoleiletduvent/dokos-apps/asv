import frappe
from frappe import throw, msgprint, _
from frappe.utils import random_string, escape_html
from asv.user import after_signup
import json

@frappe.whitelist()
def create_user_from_contact(doc: str):
    # frappe.only_for("System Manager")
    doc = json.loads(doc)
    
    prenom = doc.get("first_name")
    nom = doc.get("last_name")
    email = doc.get("email_id")

    if (not (prenom and nom and email)):
        frappe.msgprint(f"Erreur : first_name, last_name or email_id not set\nGot first_name={prenom}, last_name={nom}, email_id={email}")
        return None

    out = manual_sign_up(email, prenom, nom.upper())
    
    if out[0] == 0:
        frappe.msgprint(f"Erreur : {out[1]}")
    elif out[0] == 2:
        frappe.msgprint(f"Attention : {out[1]}")
    else:
        frappe.msgprint(f"OK : {out[1]} (code = {out[0]})")
    
    return out

@frappe.whitelist()
def create_user_from_adherent(doc: str):
    # frappe.only_for("System Manager")
    doc = json.loads(doc)
    
    prenom = doc.get("prenom")
    nom = doc.get("nom")
    email = doc.get("email")

    out = manual_sign_up(email, prenom, nom.upper())
    
    if out[0] == 0:
        frappe.msgprint(f"Erreur : {out[1]}")
    elif out[0] == 2:
        frappe.msgprint(f"Attention : {out[1]}")
    else:
        frappe.msgprint(f"OK : {out[1]} (code = {out[0]})")
    
    return out

def send_welcome_email(adh_doc, method):
    email = adh_doc.email
    first_name = adh_doc.prenom
    last_name = adh_doc.nom

    adh_doc.db_set('owner', email, commit=True)
    adh_doc.db_set('modified_by', email, commit=True)
    

    code, msg = manual_sign_up(email, first_name, last_name, ignore_permissions=True)
    after_signup.update_contact_customer_address_from_adherent(adh_doc, "")

    # print("---")
    # print(code)
    # print(msg)

# works with "Welcome email = 1"
# import importlib
# user_from_contact_email = importlib.import_module('asv.user.user_from_contact_email')
# importlib.reload(user_from_contact_email)
def manual_sign_up(email, first_name, last_name, ignore_permissions=False):
    user = frappe.db.get("User", {"email": email})
    if user:
        if user.disabled:
            return 0, _("Registered but disabled")
        else:
            return 0, _("Already Registered")
    else:
        
        user = frappe.get_doc({
            "doctype":"User",
            "name": email,
            "email": email,
            "modified_by": "Administrator",
            "owner": "Administrator",
            "language": "fr",
            "first_name": escape_html(first_name),
            "last_name": escape_html(last_name),
            "full_name": f"{first_name} {last_name}",
            "username": escape_html(f"{first_name.lower()}.{last_name.lower()}"),
            "enabled": 1,
            "new_password": random_string(10),
            "user_type": "Website User",
            "send_welcome_email": 1,
            "thread_notify": 0,
            "document_follow_notify": 0
        })
        # user.flags.ignore_permissions = True
        user.flags.ignore_password_policy = True
        print(user.as_dict())
        user.insert(
            ignore_permissions=ignore_permissions
        )

        if user.flags.email_sent:
            return 1, _("Please check your email for verification")
        else:
            return 2, _("Please ask your administrator to verify your sign-up")

# from asv.user.user_from_contact_email import insert_user_from_contact_using_email
# insert_user_from_contact_using_email('atelierdusoleiletduvent+dokostest@gmail.com')
# 
def insert_user_from_contact_using_email(email_id):
    # email_id = 'florentdpt@gmail.com'
    # email_id = 'atelierdusoleiletduvent+dokostest@gmail.com'

    customer_name = frappe.db.get_value('Customer', { 'email_id': email_id }, ['name'])
    customer = frappe.get_doc('Customer', customer_name)

    first_name, last_name = frappe.db.get_value('Contact', { 'email_id': email_id }, ['first_name', 'last_name'])

    user = frappe.get_doc({
        "doctype": "User",
        "email": customer.email_id,
        "first_name": escape_html(first_name),
        "last_name": escape_html(last_name),
        "enabled": 1,
        "new_password": random_string(10),
        "user_type": "Website User",
        "send_welcome_email": 0
    })

    user.flags.ignore_permissions = True
    user.flags.ignore_password_policy = True
    user.flags.no_welcome_mail = True
    user.flags.email_sent = True
    user.insert()
    user.notify_update()


    # u = frappe.new_doc('User')
    # u.email = customer.email_id
    # u.last_name = last_name
    # u.first_name = first_name
    # u.user_type = 'Website User'
    # u.enabled = "1"
    # # u.send_welcome_email = "1"
    # u.send_welcome_email = "0"

    # u.insert()
    # u.notify_update()