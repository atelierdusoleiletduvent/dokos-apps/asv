import frappe

# def update_contact_customer_address_from_adherent(email):
def update_contact_customer_address_from_adherent(doc, method):
    # frappe.msgprint(doc.as_dict())
    # user = frappe.get_doc('User', f"{doc.as_dict()}")
    adh = doc
    # user = frappe.get_doc('User', adh.email)

    
    adh_name = f"{adh.prenom}_{adh.nom}"
    # adh_name = f"{user.first_name}_{user.last_name}"
    # adh_name = f"DokosTest_ASV2023"

    adh_doc = frappe.get_doc('Adherent', adh_name)
    
    first_name = adh_doc.prenom
    last_name = adh_doc.nom

    add = frappe.get_doc({
        'doctype': 'Address',
        'owner': adh_doc.email,
        'modified_by': adh_doc.email,
        'address_title' : f"{first_name} {last_name}",
        'address_type' : 'Billing',
        'address_line1': adh_doc.adresse_ligne_1,
        'address_line2': adh_doc.adresse_complement,
        'city': adh_doc.adresse_ville,
        'pincode': adh_doc.adresse_cp,
        'country': "France",
        'email_id': adh_doc.email,
        'phone': adh_doc.telephone,
        'is_primary_address': 1,
    })

    add.insert(ignore_permissions=True)

    cust = frappe.get_doc({
        'doctype': 'Customer',
        'owner': adh_doc.email,
        'modified_by': adh_doc.email,
        'customer_name' : f"{first_name} {last_name}",
        'customer_type' : 'Individual',
        'customer_group': 'Individuel',
        'territory': 'Tous les territoires',
        'customer_primary_contact': f"{first_name} {last_name}",
        'email_id': adh_doc.email,
        'mobile_no': adh_doc.telephone,
        'customer_primary_address': add.name,
    })

    cust.insert(ignore_permissions=True)

    # cust.db_set('customer_primary_address', add.name, commit=True)

    # contact.db_set('address', add.address_title)
    contact = frappe.get_doc({
        "doctype": "Contact", 
        "name": f"{first_name} {last_name}",
        "first_name": first_name,
        "last_name": last_name,
        "email_id": adh_doc.email,
        "user": adh_doc.email,
        "phone": adh_doc.telephone,
        "mobile_no": adh_doc.telephone,
        "address": add.name,
        "is_primary_contact": 1,
        "is_billing_contact": 1,
        "owner": 'Administrator',
        "modified_by": 'Administrator',
        
    })

    # contact.is_primary_contact = 1
    # contact.is_billing_contact = 1
    contact.append('email_ids', {
        'email_id': adh_doc.email,
        'is_primary': 1,
    })
    # contact.db_update()
    contact.append('phone_nos', {
        'phone': adh_doc.telephone,
        'is_primary_phone': 1,
        'is_primary_mobile_no': 1,
    })
    # contact.db_update()
    # contact.address = add.name
    # frappe.db.commit()
    # contact.reload()
    # contact.db_set('address', add.name, commit=True)
    # contact.db_set('owner', 'Administrator', commit=True)
    # contact.db_set('modified_by', 'Administrator', commit=True)
    contact.owner = 'Administrator'
    contact.modified_by = 'Administrator'
    
    contact.insert(ignore_permissions=True)
    # contact.db_update()

    # ADDRESS <-> CUSTOMER
    dl_data = {
        'doctype': 'Dynamic Link',
        'owner': adh_doc.email,
        'modified_by': adh_doc.email,
        'idx': 1,
        "parenttype": "Address",
        "parentfield": "links",
        "parent": add.name,
        "link_doctype": "Customer",
        "link_name": f"{first_name} {last_name}",
        "link_title": f"{first_name} {last_name}",
    }

    dl_exists = frappe.db.exists(dl_data)

    if not dl_exists: 
        dl = frappe.get_doc(dl_data)
        dl.insert(ignore_permissions=True)

    # CONTACT <-> CUSTOMER
    dl2_data = {
        'doctype': 'Dynamic Link',
        'owner': adh_doc.email,
        'modified_by': adh_doc.email,
        'idx': 1,
        "parenttype": "Contact",
        "parentfield": "links",
        "parent": contact.name,
        "link_doctype": "Customer",
        "link_name": f"{first_name} {last_name}",
        "link_title": f"{first_name} {last_name}",
    }

    dl2_exists = frappe.db.exists(dl2_data)

    if not dl2_exists: 
        dl2 = frappe.get_doc(dl2_data)
        dl2.insert(ignore_permissions=True)
    
    frappe.db.commit()