import frappe

def statut_adhesion_for_adherent(year, email, cname):
  
  si_dict = sales_invoices_status(year, email, cname)
  si_status = si_to_adh_status(si_dict)

  so_dict = sales_orders_status(year, email, cname)
  so_status = so_to_adh_status(so_dict)

  debug = {
    'invoices': si_dict, 
    'status_invoice_based': si_status,
    'orders': so_dict,
    'status_order_based': so_status,
  }
  
  if not si_dict or not so_dict:
    print("EMPTY ?")
    print(si_dict)
    print(so_dict)

  if si_status == so_status:
    
    if si_status == "unknown":
      return (so_status, debug)
    
    else:
      return (si_status, debug)

  else:

    if so_status == "no":
      return (si_status, debug)

    elif si_status == "no":
      return (so_status, debug)
    
    elif si_status == "yes" and so_status == "unknown":
      return (si_status, debug)

    elif si_status == "awaiting_payment" and so_status == "unknown":
      return (si_status, debug)

    else:
      return ("unknown", debug)
    

def si_to_adh_status(si_dict):

  paid_invoices = si_dict.get('paid_invoices')  or []
  returned_invoices = si_dict.get('returned_invoices')  or []
  unpaid_invoices = si_dict.get('unpaid_invoices')  or []
  paid_not_returned_invoices = si_dict.get('paid_not_returned_invoices')  or []

  adh_status = "unknown"

  if len(unpaid_invoices) == 0:
    
    if len(paid_not_returned_invoices) == 1:
      adh_status = "yes"
    
    elif len(paid_not_returned_invoices) == 0:
      adh_status = "no"
    
    else:
      adh_status = "unknown"

  elif len(unpaid_invoices) == 1:

    if len(paid_not_returned_invoices) == 0:
      adh_status = "awaiting_payment"
    
    else:
      adh_status = "unknown"
  
  else:
      adh_status = "unknown"

  return adh_status
  
def sales_invoices_status(year, email, cname):
  values = {'item_code': f"ADH-{year}", 'email_id': email, 'customer_name': cname}

  data = frappe.db.sql("""
  SELECT 
    si.name, 
    sii.name as "sii_name", 
    c.name as "customer_name", 
    c.email_id, 
    si.docstatus, 
    si.is_return, 
    si.return_against, 
    si.is_down_payment_invoice, 
    si.status, 
    si.grand_total, 
    si.outstanding_amount, 
    si.base_paid_amount, 
    sii.item_code, 
    sii.sales_order
  FROM `tabSales Invoice` as si
    LEFT JOIN
    `tabSales Invoice Item` as sii
    ON sii.parenttype = "Sales Invoice" AND sii.parentfield = "items" AND sii.parent = si.name 
    LEFT JOIN
    `tabCustomer` as c
    ON c.name = si.customer
  WHERE sii.item_code = %(item_code)s AND c.email_id = %(email_id)s AND c.name = %(customer_name)s
  ORDER BY c.name, name
  """, values=values, as_dict=1)

  paid_invoices = []
  returned_invoices = []
  unpaid_invoices = []

  for d in data:
    # print(d)
    
    # récupere la liste des factures payées (acomptes ou finales) avec ADH-2023
    if d.status == "Paid" and d.is_return == 0:
      paid_invoices.append(d.name)

    # récupere la liste des factures remboursées avec ADH-2023
    if d.status == "Return" and d.is_return == 1:
      returned_invoices.append({"returned": d.name, "against": d.return_against})
    if d.status == "Credit Note Issued":
      returned_invoices.append({"returned": d.name, "against": "???"})

    # récupere la liste des factures impayées (acomptes ou finales) avec ADH-2023
    if ("Overdue" in d.status or "Unpaid" in d.status) and d.is_return == 0:
      unpaid_invoices.append(d.name)

  paid_not_returned_invoices = paid_invoices
  for r in returned_invoices:
    if r.get('against') in paid_not_returned_invoices:
      paid_not_returned_invoices.remove(r.get('against'))

  # DEBUG / PRINT

  # print("PAID invoices :")
  # for p in paid_invoices:
    # print(p)
  
  # print("RETURNED invoices :")
  # for p in returned_invoices:
    # print(p)

  # print("UNPAID invoices :")
  # for p in unpaid_invoices:
    # print(p)
  
  # print("PAID NOT RETURNED invoices :")
  # for p in paid_not_returned_invoices:
  #   print(p)

  return {
    'paid_invoices': paid_invoices,
    'returned_invoices': returned_invoices,
    'unpaid_invoices': unpaid_invoices,
    'paid_not_returned_invoices': paid_not_returned_invoices
  }


def so_to_adh_status(so_status_dict):
  
  paid = so_status_dict.get('paid') or []
  prepaid = so_status_dict.get('prepaid') or []
  unprepaid = so_status_dict.get('unprepaid') or []
  unknown = so_status_dict.get('unknown') or []
  closed_cancelled = so_status_dict.get('closed_cancelled') or []

  flagged = so_status_dict.get('flagged') or []

  adh_status = "no"

  if len(flagged) >= 1:
    adh_status = "unknown"
  
  else:

    if len(paid) == 1:

      if len(prepaid) == 0 and len(unprepaid) == 0 and len(unknown) == 0:
        adh_status = "yes"
      else:
        adh_status = "unknown"

    elif len(prepaid) == 1:

      if len(unprepaid) == 0 and len(unknown) == 0 and len(closed_cancelled) == 0:
        adh_status = "prepaid"
      else:
        adh_status = "unknown"
    
    elif len(unprepaid) == 1:

      if len(paid) == 0 and len(prepaid) == 0 and len(unknown) == 0 and len(closed_cancelled) == 0:
        adh_status = "awaiting_payment"
      else:
        adh_status = "unknown"

    elif len(unknown) != 0:
      adh_status = "unknown"
    
    else:
      adh_status = "no"
    
  return adh_status


def sales_orders_status(year, email, cname):
  
  values = {'item_code': f"ADH-{year}", 'email_id': email, 'customer_name': cname}
  
  data = frappe.db.sql("""
  SELECT 
    so.name, 
    c.name as "customer_name", 
    so.grand_total, 
    so.advance_paid, 
    so.status,
    so.billing_status, 
    soi.item_code
  FROM `tabSales Order` as so
    LEFT JOIN
      `tabSales Order Item` as soi
      ON soi.parenttype = "Sales Order" AND soi.parentfield = "items" AND soi.parent = so.name 
      LEFT JOIN
      `tabCustomer` as c
      ON c.name = so.customer
  WHERE 
    soi.item_code = %(item_code)s AND 
    c.email_id = %(email_id)s AND 
    c.name = %(customer_name)s
  """, values=values, as_dict=1)

  flag_data = frappe.db.sql("""
  SELECT sii.name as "sii_name", si.is_return, si.return_against, so.name, c.name as "customer_name", so.grand_total, so.advance_paid, so.status, so.billing_status, soi.item_code
    FROM `tabSales Order` as so
        LEFT JOIN
        `tabSales Order Item` as soi
        ON soi.parenttype = "Sales Order" AND soi.parentfield = "items" AND soi.parent = so.name 
        LEFT JOIN
        `tabCustomer` as c
        ON c.name = so.customer
        LEFT JOIN
        `tabSales Invoice Item` as sii
        ON sii.sales_order = so.name
        LEFT JOIN
        `tabSales Invoice` as si
        ON sii.parenttype = "Sales Invoice" AND sii.parentfield = "items" AND sii.parent = si.name 
  WHERE 
    soi.item_code = %(item_code)s AND 
    c.email_id = %(email_id)s AND 
    c.name = %(customer_name)s AND
    si.is_return = 1
  ORDER BY
      so.name
    """, values=values, as_dict=1)

  paid = []
  prepaid = []
  unprepaid = []
  unknown = []
  closed_cancelled = []

  flagged = []

  for d in data:
    if d.billing_status == "Fully Billed":
      paid.append(d.name)
    
    elif d.billing_status == "Not Billed":
    
      if d.status == "On Hold" and d.advance_paid == d.grand_total:
        prepaid.append(d.name)
      
      elif d.status == "To Bill":
        
        if d.advance_paid < d.grand_total:
          unprepaid.append(d.name)
        elif d.advance_paid == d.grand_total:
          prepaid.append(d.name)
      
      elif d.status == "Closed" or d.status == "Cancelled":
        closed_cancelled.append(d.name)

      else:
        unknown.append(d.name)
    else:
      unknown.append(d.name)
  
  # flag si :
  # - existance de factures de remboursement liées à la commande client (acompte ou finale)
  # pas de flag si :
  # - la commande client est "Closed" or "Completed" puisque dans ce cas, les factures de ventes reflètent la réalité
  # si flag & pas de sales invoices alors "unknown"
  # si flag & sales invoices alors sales_invoices (?)
  for d in flag_data:
    if d.status == "To Bill" or d.status == "On Hold":
      flagged.append(d.name)

  return {
    'paid': paid,
    'prepaid': prepaid,
    'unprepaid': unprepaid,
    'unknown': unknown,
    'closed_cancelled': closed_cancelled,
    'flagged': flagged
  }


def get_invoices_for_customer(email, cname):
  cnames = frappe.get_doc("Sales Invoices", 
    filters={
      "email_id": email,
      "customer": cname,
    },
    pluck="name")

  [ frappe.get_doc("Customer", cname) for cname in cnames ]

def get_customers_by_email(email):
  cnames = frappe.get_list("Customer", 
    filters={
      "email_id": email,
    },
    pluck="name")

  [ frappe.get_doc("Customer", cname) for cname in cnames ]


  