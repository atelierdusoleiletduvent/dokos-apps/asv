function rot13(str) {
    var input     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var output    = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm';
    var index     = x => input.indexOf(x);
    var translate = x => index(x) > -1 ? output[index(x)] : x;
    return str.split('').map(translate).join('');
}

function rot5(str) {
    var input     = '0123456789';
    var output    = '5678901234';
    var index     = x => input.indexOf(x);
    var translate = x => index(x) > -1 ? output[index(x)] : x;
    return str.split('').map(translate).join('');
}

frappe.ready(() => {

    // fot 'stage' pages
    var els = document.getElementsByClassName("rot-enc")    							
    for (var i=0; i < els.length; i++) {
        var el = els[i];
        var enc = el.dataset.enc;
        var dec = "";
        if (el.classList.contains("email")) {
            var dec = rot13(enc);
            el.setAttribute("href", "mailto:"+dec);
        }
        if (el.classList.contains("tel")) {
            var dec = rot5(enc);
            el.setAttribute("href", "tel://"+dec);
        }
        el.innerHTML = dec;
    }

    // for 'contact' page
    var contact_tel_el = document.querySelector("span[itemprop='telephone'") 
    if (contact_tel_el) {
        contact_tel_el.textContent = rot5(contact_tel_el.textContent);
    }

    var contact_email_el = document.querySelector("span[itemprop='email'") 
    if (contact_email_el) {
        contact_email_el.textContent = rot13(contact_email_el.textContent);
    }
    
});

// frappe.ready(() => {
//     frappe.require('/assets/asv/js/utils.js', () => {
//         var els = document.getElementById("website_content").getElementsByClassName("rot-enc")    							
//         for (var i=0; i < els.length; i++) {
//             var el = els[i];
//             var enc = el.dataset.enc;
//             var dec = "";
//             if (el.classList.contains("email")) {
//                 var dec = rot13(enc);
//                 el.setAttribute("href", "mailto:"+dec);
//             }
//             if (el.classList.contains("tel")) {
//                 var dec = rot5(enc);
//                 el.setAttribute("href", "tel://"+dec);
//             }
//             el.innerHTML = dec;
//         }
//     });
// })