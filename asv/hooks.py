from . import __version__ as app_version

app_name = "asv"
app_title = "ASV"
app_publisher = "Guillaume AUGAIS"
app_description = "Atelier du Soleil et du Vent"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "atelierdusoleiletduvent@gmail.com"
app_license = "MIT"

# translated_languages_for_website = []

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/asv/css/asv.css"
# app_include_js = "/assets/asv/js/asv.js"

# include js, css files in header of web template
web_include_css = "/assets/asv/css/asv.css"
web_include_js = "/assets/asv/js/asv.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "asv/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------
# add methods and filters to jinja environment
# jinja = {
# 	"methods": "asv.utils.jinja_methods",
# 	"filters": "asv.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "asv.install.before_install"
# after_install = "asv.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "asv.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes
# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Adherent": {
		"after_insert": "asv.user.user_from_contact_email.send_welcome_email",
	},
	"Item": {
		"on_update": "asv.item.on_item_update.update_info_with_stage_details",
	},
	"Quotation": {
		# "after_insert": "asv.devis.after_insert.notify_by_email_www",
		"before_save": "asv.devis.before_insert.insert_adh_if_needed",
		"on_update": 	[
			# "asv.devis.before_insert.insert_adh_if_needed",
			"asv.devis.after_insert.notify_by_email_www",
			"asv.devis.after_insert.notify_user_after_submit",
			"asv.devis.after_insert.notify_asv_after_submit",
		]
	},
	"Stage": {
		"on_update": [
			"asv.stage.on_stage_update.set_encoded_courriel_and_tel",
			"asv.stage.on_stage_update.trigger_item_update"
		]
	}
}

# Scheduled Tasks
# ---------------

scheduler_events = {
	"cron" : {
		"*/5 * * * *" : [
			"asv.stage.helpers.update_cache_dates_des_stages_a_venir",
			"asv.www.calendrier-stages.index.update_cache_calendrier_stages"
		]
	}
}
# scheduler_events = {
# 	"all": [
# 		"asv.tasks.all"
# 	],
# 	"daily": [
# 		"asv.tasks.daily"
# 	],
# 	"hourly": [
# 		"asv.tasks.hourly"
# 	],
# 	"weekly": [
# 		"asv.tasks.weekly"
# 	],
# 	"monthly": [
# 		"asv.tasks.monthly"
# 	],
# }

# Testing
# -------

# before_tests = "asv.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "asv.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "asv.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]

# User Data Protection
# --------------------
# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

# Authentication and authorization
# --------------------------------
# auth_hooks = [
# 	"asv.auth.validate"
# ]

