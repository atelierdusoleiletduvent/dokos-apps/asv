import frappe
from datetime import datetime
import locale

def today_early():
    now = datetime.now()
    early = now.replace(hour=2, minute=0, second=0, microsecond=0)
    return early

def format_creneau_pretty(creneau):
    old_locale = locale.getlocale()
    locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF-8'))
    out = creneau.strftime('%A %-d %B')
    locale.setlocale(locale.LC_ALL, old_locale)
    return out

def format_horaire_pretty(creneau):
    return creneau.strftime('%H:%M')

def format_horaires_pretty(creneau_deb, creneau_fin):
    out = format_horaire_pretty(creneau_deb) + ' - ' + format_horaire_pretty(creneau_fin)
    return out

def generate_atoc_dict(c):
    return {
        "name":"ASV / Ouverture Atelier",
        "label":"Ajouter au Calendrier",
        "description":"Atelier du Soleil et du Vent<br>Ateliers partagés bois & métal<br>Consulter les détails du créneau d'ouverture (référent, accompagnement, statut) ici → [url]https://www.atelierdusoleiletduvent.org/calendrier-ateliers[/url]",
        "startDate": c.date_js ,
        "endDate": c.date_js ,
        "startTime": c.horaire_debut_js,
        "endTime": c.horaire_fin_js,
        "location":"57 avenue de Poitiers 86600 Lusignan",
        "options":[
        "Apple",
        "Google",
        "iCal",
        "Microsoft365",
        "MicrosoftTeams",
        "Outlook.com",
        "Yahoo"
        ],
        "timeZone":"Europe/Paris",
        "iCalFileName":"ASV-Ouverture-Atelier"
    }

# import importlib
# index = importlib.import_module('asv.www.calendrier-ateliers.index')
# importlib.reload(index)
def get_creneaux():
    cs = frappe.get_all('Creneau Ouverture Atelier', 
        filters = {
            'creneau_debut': ['>=', today_early()]
        },
        fields=['name', 'creneau_debut', 'creneau_fin', 'intitule', 'statut', 'referent', 'accompagnement_possible'],
        order_by='creneau_debut asc',
    )

    out = []

    for c in cs:
        el = c
        
        el['creneau_pretty'] = format_creneau_pretty(c.creneau_debut)
        el['horaires_pretty'] = format_horaires_pretty(c.creneau_debut, c.creneau_fin)
        
        el['date_js'] = c.creneau_debut.strftime('%Y-%m-%d')
        el['horaire_debut_js'] = format_horaire_pretty(c.creneau_debut)
        el['horaire_fin_js'] = format_horaire_pretty(c.creneau_fin)
        
        el['atoc_dict'] = "{'name':'test'}" # generate_atoc_dict(el)
        el['atoc_data'] = generate_atoc_dict(el)

        color = 'inherit'
        if 'Confirmé' in c.statut:
            color = '#66bb93'
        elif 'Non confirmé' in c.statut:
            # color = '#e8e654'
            color = '#dd6d2f'
        elif 'Annulé' in c.statut:
            color = '#dd6d2f'
        else:
            color = 'inherit'
        el['color'] = color

        el['creneau'] = c

        qualifs_acc = {}

        accs = frappe.get_all('Accompagnateur Atelier Ouvert', 
            filters = {
                'parent': c.name,
                'parentfield': 'accompagnateurs',
                'parenttype': 'Creneau Ouverture Atelier',
            },
            fields=['name', 'nom_affiche', 'passeport', 'details'],
        )
        
        accs_details = []
        for acc in accs:
            pssprt = frappe.get_doc('Passeport Atelier', acc.passeport)

            # ajoute l'équipement à la liste des qualifs en accompagnement présentes
            for q in pssprt.qualifications:
                if q.accompagnateur == 1:
                    d = q.as_dict()
                    if d.type_equipement not in qualifs_acc:
                        qualifs_acc[d.type_equipement] = []
                    if d.equipement not in qualifs_acc[d.type_equipement]:
                        qualifs_acc[d.type_equipement].append(d.equipement) 

            acc['passeport_details'] = pssprt.as_dict()
            accs_details.append(acc)
            # print(f'name={acc.name} - nom_affiche={acc.nom_affiche}')

        el['accompagnateurs'] = accs_details

        prtcpts = frappe.get_all('Participant Atelier Ouvert', 
            filters = {
                'parent': c.name,
                'parentfield': 'participants',
                'parenttype': 'Creneau Ouverture Atelier',
            },
            fields=['name', 'nom_affiche', 'projet', 'adherent'],
        )    

        prtcpts_details = []
        for prtcpt in prtcpts:
            pssprt = frappe.get_doc('Passeport Atelier', prtcpt.adherent)

            # ajoute l'équipement à la liste des qualifs en accompagnement présentes
            for q in pssprt.qualifications:
                if q.accompagnateur == 1:
                    d = q.as_dict()
                    if d.type_equipement not in qualifs_acc:
                        qualifs_acc[d.type_equipement] = []
                    if d.equipement not in qualifs_acc[d.type_equipement]:
                        qualifs_acc[d.type_equipement].append(d.equipement)

            prtcpt['adherent_details'] = pssprt.as_dict()
            prtcpts_details.append(prtcpt)

        el['participants'] = prtcpts_details
        el['qualifs_accompagnement'] = qualifs_acc

        out.append(el)

    return out

def get_context(context):
    # cs = 'test'
    
    context.cs = get_creneaux()
    context.no_cache = 1

    return context

@frappe.whitelist(allow_guest=True)
def export_as_json_for_fullcalendar_events_model():
    out = []
    # stages_list = retrieve_cache_calendrier_stages()
    creneaux = get_creneaux()
    
    for c in creneaux:
        start_full = "%sT%s:00" % (c['date_js'], c['horaire_debut_js'])
        end_full = "%sT%s:00" % (c['date_js'], c['horaire_fin_js'])
        
        ev_dict = {
            'title': c['intitule'],
            'start': start_full,
            'end': end_full,
            'url': '/calendrier-ateliers',
            'editable': False,
        }
        out.append(ev_dict)
    
    return out