import frappe
from datetime import datetime
import locale
from asv.stage.helpers import get_iscdp_arr, format_tarifs_as_str, format_to_pretty_date
from asv.item.on_item_update import parse_start_and_end_date
import json

def reserved_actual_max_min_qty_for(stage, item):
    item_code = item.item_code
    
    xs = frappe.db.get_all('Bin', filters={'item_code': item_code}, fields=['item_code', 'reserved_qty', 'actual_qty']) 

    reserved_tot = 0
    actual_tot = 0

    for x in xs:
        reserved_tot = reserved_tot + x['reserved_qty']
        actual_tot = actual_tot + x['actual_qty']

    max_qty = stage.participants_max
    min_qty = stage.participants_min
    
    out = {
        'disabled': (item.disabled if item else 1),
        'reserved' : reserved_tot if xs else 0,
        'actual': actual_tot if xs else 0,
        'max' : max_qty,
        'min' : min_qty
    }

    return out

def get_item_model_stage(item_code):
    item = frappe.get_doc('Item', item_code)
    item_model = frappe.get_doc('Item', item.variant_of)
    stage = frappe.get_doc('Stage', item_model.item_name)

    return {
        'item': item,
        'model': item_model,
        'stage': stage
    }

def statut(dram_dict):
    status = ""

    actual = dram_dict['actual']
    min = dram_dict['min']
    max = dram_dict['max']
    reserved = dram_dict['reserved']

    max_or_actual = actual if actual != 0 else max

    if (not dram_dict['disabled'] and reserved >= max_or_actual):
        status = "complet avec %dp" % (reserved)
    elif (dram_dict['disabled'] and reserved < max_or_actual):
        status = "reporté / annulé"
    elif (not dram_dict['disabled'] and reserved < max_or_actual):
        reste = max_or_actual - reserved if (max_or_actual - reserved) > 0 else 0
        # status = "reste %dp" % (reste)
        status = "reste %dp (%d à %d requis)" % (reste, min, max)
    
    return status

def format_date_js(dt):
    return dt.strftime('%Y-%m-%d')

def format_horaire_js(dt):
    return dt.strftime('%H:%M')

def later_than(dt=datetime.today()):
    def filter_function(el):
        start, end = parse_start_and_end_date(el.attribute_value)
        return start >= dt
    return filter_function

def sort_by_start_date(el):
    _, end = parse_start_and_end_date(el.attribute_value)
    return end

def get_stage_item_codes_beginning_after(dt=datetime.today()):
    xs = frappe.db.get_list('Item Variant Attribute', 
        filters={
            'attribute': 'Date',
            'parenttype': 'Item',
            'attribute_value': ('!=', '')
        }, 
        fields=['name', 'attribute', 'attribute_value', 'variant_of', 'parenttype', 'parent']
    )
    
    fs = filter(later_than(dt), xs)
    ss = sorted(fs, key=sort_by_start_date)
    
    return [ s['parent'] for s in ss ]

def get_date_attribute_and_parsed_for(item_code):
    all_date_variants = frappe.db.get_list('Item Variant Attribute', 
        filters={
            'parent': item_code,
            'attribute': 'Date',
            'parenttype': 'Item'
        }, 
        fields=['name', 'attribute', 'attribute_value', 'variant_of', 'parenttype', 'parent']
    )
    
    attr_and_parsed = [ 
        {
            'attribute_value': dv.attribute_value, 
            'start_parsed': parse_start_and_end_date(dv.attribute_value)[0],
            'end_parsed': parse_start_and_end_date(dv.attribute_value)[1] 
        }
        for dv in all_date_variants 
        if (dv.attribute_value is not None)
    ]
    assert(len(attr_and_parsed) == 1)

    return attr_and_parsed[0]

def parse_horaire_stage(horaire_pretty):
    dt = None
    try:
        if (horaire_pretty.endswith("h")):
            dt = datetime.strptime(horaire_pretty, '%Hh')
        else:
            dt = datetime.strptime(horaire_pretty, '%Hh%M')
    except ValueError as e:
        frappe.msgprint(f"Erreur : format d'horaire invalide '{horaire_pretty}'. Attendu : '6h' ou '6h30'")
    return dt

def generate_atoc_dict(d):
    # <button id="default-button">Add to calendar</button>
    # <script type="application/javascript">
    # const config = {
    #     name: "Reminder to check the add-to-calendar-button repo",
    #     description: "Check out the maybe easiest way to include add-to-calendar-buttons to your website at:<br>→ [url]https://github.com/jekuer/add-to-calendar-button[/url]",
    #     startDate: "2022-01-14",
    #     endDate: "2022-01-18",
    #     options: ["Google", "iCal"],
    #     timeZone: "Europe/Berlin",
    #     trigger: "click",
    #     iCalFileName: "Reminder-Event",
    # }
    # const button = document.querySelector('#default-button')
    # button.addEventListener('click', () => atcb_action(config, button))
    # </script>
    return {
        "name": d['titre'],
        "label": "Ajouter au calendrier",
        "description": f"Détails & Inscriptions<br> → [url]{d['url']}[/url]<br>Atelier du Soleil et du Vent",
        "startDate": d['date_debut_js'],
        "endDate": d['date_fin_js'],
        "startTime": d['horaires_debut_js'],
        "endTime": d['horaires_fin_js'],
        "location": d['lieu'],
        "options":[
        "Apple",
        "Google",
        "iCal",
        "Microsoft365",
        "MicrosoftTeams",
        "Outlook.com",
        "Yahoo"
        ],
        "timeZone":"Europe/Paris",
        "trigger": "click",
        "iCalFileName":f"{d['titre']} - {d['date_pretty']}",
    }

def evenement_stage(item_code):

    ims = get_item_model_stage(item_code)
    
    item = ims['item']
    model = ims['model']
    stage = ims['stage']
    

    dram_dict = reserved_actual_max_min_qty_for(stage, item)
    status = statut(dram_dict)

    date_dict = get_date_attribute_and_parsed_for(item_code)
    date_pretty = date_dict['attribute_value']

    horaire_debut = parse_horaire_stage(stage.horaires_debut)
    horaire_fin  = parse_horaire_stage(stage.horaires_fin)

    d = {
        'categorie': 'Stages et Formations',
        # 'titre': f'{model.item_name} - {date_pretty} ({status})',
        'item_code': item_code,
        'titre': f'{model.item_name}',
        'date_pretty': format_to_pretty_date(date_dict['end_parsed'], date_pretty),
        'date_debut_js': format_date_js(date_dict['start_parsed']),
        'horaire_debut_stage': stage.horaires_debut,
        'horaires_debut_js': format_horaire_js(horaire_debut),
        'date_fin_js': format_date_js(date_dict['end_parsed']),
        'horaire_fin_stage': stage.horaires_fin,
        'horaires_fin_js': format_horaire_js(horaire_fin),
        'lieu': stage.lieu,
        'url': f'https://www.atelierdusoleiletduvent.org/{model.route}',
        'details': '',
        'statut': status,
        'tarifs': format_tarifs_as_str(stage),
        'acces': 'sur réservation'
    }

    return d


# import importlib
# index = importlib.import_module('asv.www.calendrier-stages.index')
# index.update_cache_calendrier_stages()
def update_cache_calendrier_stages():
    d = get_stages_list()
    j = json.dumps(d)
    frappe.db.set_value('ASV Cache Formation', 'calendrier_stages', 'value', j)
    frappe.db.commit()

def retrieve_cache_calendrier_stages():
    j = frappe.db.get_value('ASV Cache Formation', 'calendrier_stages', 'value')
    stages_list = json.loads(j)
    return stages_list

def get_stages_list():

    stage_item_codes = get_stage_item_codes_beginning_after(datetime.today())

    stages_list = []
    for icode in stage_item_codes:
        d = evenement_stage(icode)
        d['atoc_data'] = generate_atoc_dict(d)
        if d['statut'] != "reporté / annulé" and d['statut'] != "":
            stages_list.append(d)
    
    return stages_list

@frappe.whitelist(allow_guest=True)
def export_as_json_for_fullcalendar_events_model():
    out = []
    stages_list = retrieve_cache_calendrier_stages()
    
    for d in stages_list:
        start_full = "%sT%s:00" % (d['date_debut_js'], d['horaires_debut_js'])
        end_full = "%sT%s:00" % (d['date_fin_js'], d['horaires_fin_js'])
        
        libelle = d['titre'] + ' // ' + d['statut']

        ev_dict = {
            'id': d['item_code'],
            'title': libelle,
            'start': start_full,
            'end': end_full,
            'url': d['url'],
            'editable': False,
        }
        out.append(ev_dict)
    
    return out

def get_context(context):
    stages_list = retrieve_cache_calendrier_stages()
    
    context.stages = stages_list
    context.no_cache = 1

    return context