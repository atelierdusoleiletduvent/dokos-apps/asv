import frappe
from asv.item.on_item_update import update_info_with_stage_details, make_context_for_rendering, fetch_related_stage, parse_date, parse_regular_date
import locale
from datetime import timedelta, date, datetime
from bs4 import BeautifulSoup
import json

def format_to_pretty_date(date, pretty):
    old_locale = locale.getlocale()
    locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF-8'))

    out = ""

    if pretty.startswith("Week-end du "):
        sunday = date
        saturday = date - timedelta(days=1)
        
        sunday_str = sunday.strftime('%-d')
        saturday_str = saturday.strftime('%-d')

        month_str = sunday.strftime('%B')

        out =  f"Week-end du {saturday_str}-{sunday_str} {month_str}"
    else:
        out = date.strftime('%A %-d %B')

    locale.setlocale(locale.LC_ALL, old_locale)

    return out


def get_date_interval(from_date_pretty, to_date_pretty):
    old_locale = locale.getlocale()
    locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF-8'))

    from_date = parse_regular_date(from_date_pretty)
    to_date = parse_regular_date(to_date_pretty)

    locale.setlocale(locale.LC_ALL, old_locale)

    return (from_date, to_date)

def _extract_iscdp_arr():
    _all = frappe.get_all('Item', ['name', 'item_group', 'item_name'])
    # print(_all)
    
    item_names = []
    for item in _all:
        if 'Stages' in item.item_group:
            item_names.append(item.name)
    
    items = [ frappe.get_doc('Item', item_name) for item_name in item_names ]
    stages = [ fetch_related_stage(item.name) for item in items ]
    
    zipped_is = list(zip(items, stages))
    
    contexts = [ make_context_for_rendering(i, s) for i,s in zipped_is ]
    
    pretty_dates_array = [ c['dates'] for c in contexts ]
    raw_dates_array = [ [ parse_date(pretty_d) for pretty_d in pretty_dates ] for pretty_dates in pretty_dates_array ]

    zipped = list(zip(items, stages, contexts, raw_dates_array, pretty_dates_array))

    sorted_arr = sorted(zipped, key=lambda z: min(z[3]) if len(z[3]) > 0 else datetime(2099, 12, 31, 0, 0))

    # print("=============== sorted_arr =================")
    # print(sorted_arr)
    return sorted_arr

def _filter_iscdp_arr_by_date(iscdp_arr, from_date, to_date):

    filtered_arr = []
    for i, s, c, dates, pretties in iscdp_arr:
        ds = []
        ps = []
        
        for d, p in zip(dates, pretties):
            if from_date <= d and d <= to_date:
                ds.append(d)
                ps.append(p)
        
        filtered_arr.append((i, s, c, ds, ps))
    
    # print("=============== filtered_arr =================")
    # print(filtered_arr)

    return filtered_arr

def _filter_iscdp_arr_by_stages(iscdp_arr, only_stage_names):
    filtered_arr = []
    
    if len(only_stage_names) > 0:
        for i, s, c, ds, ps in iscdp_arr:
            if s and s.titre_court in only_stage_names:
                filtered_arr.append((i, s, c, ds, ps))
    else:
        filtered_arr = iscdp_arr
    
    return filtered_arr

def _filter_iscdp_arr_only_active(iscdp_arr):
    filtered_arr = []
    
    for i, s, c, dates, pretties in iscdp_arr:
        if c['description'] and len(dates) > 0:
            filtered_arr.append((i, s, c, dates, pretties))
    
    return filtered_arr

def format_dates_array_as_str(dates, pretties):
    formatted_dates = [ format_to_pretty_date(d, p) for d, p in list(zip(dates, pretties)) ]
    formatted_dates_str = " / ".join(formatted_dates)
    return formatted_dates_str

def format_tarifs_as_str(stage):
    tarifs_str = ""
    if stage.formule_tarifaire == 'prix fixe':
        tarifs_str = ("%s" % str(stage.tarif_reservation))
    elif stage.formule_tarifaire == 'prix fixe ou prix libre':
        tarifs_str = ("%s" % str(stage.tarif_reservation))
    else: # prix libre
        tarifs_str = ("à partir de %s" % str(stage.tarif_reservation))
    
    tarifs_str = "gratuit (adhésion annuelle demandée)" if tarifs_str == "0€" else tarifs_str
    
    return tarifs_str

def parse_html_and_format(html):
    elem = BeautifulSoup(html, features="html.parser")
    text = ''
    for e in elem.descendants:
        if isinstance(e, str):
            text += e.strip()
        elif e.name in ['br', 'p', 'h1', 'h2', 'h3', 'h4','tr', 'th']:
            text += '\n\n'
        elif e.name == 'li':
            text += '\n- '
    return text

def format_encadrants(encadrants):
    str_arr = [ f"""{e.prenom} {e.nom.upper()} ({BeautifulSoup(e.organisation, features="html.parser").get_text()}) : {e.qualifications}""" for e in encadrants ]
    return "- " + "\n- ".join(str_arr)

# PRINT FORMATS

def _print_iscdp_flyer(iscdp):
    i, s, c, dates, pretties = iscdp
    formatted_dates_str = format_dates_array_as_str(dates, pretties)
    tarifs_str = format_tarifs_as_str(s)
    
    return (f"""{{ 
'titre': "{s.titre_court}",
'dates_et_details': "{formatted_dates_str}",
'tarifs': '{tarifs_str}'
}},""") 

def _print_iscdp_facebook_short(iscdp):
    i, s, c, dates, pretties = iscdp
    formatted_dates_str = format_dates_array_as_str(dates, pretties)
    tarifs_str = format_tarifs_as_str(s)
    
    return (f"""
👉 {s.titre_court}
https://atelierdusoleiletduvent.org/{i.route}
{formatted_dates_str}""")

def _print_iscdp_facebook_long(iscdp):
    i, s, c, dates, pretties = iscdp
    formatted_dates_str = format_dates_array_as_str(dates, pretties)
    tarifs_str = format_tarifs_as_str(s)
    
    return (f"""
[STAGE // {s.titre_court}]
{s.resume}

Prochaines dates :
👉 {formatted_dates_str}

https://www.atelierdusoleiletduvent.org/{i.route}

Lieu : {s.lieu}
Durée : {s.duree}
Tarif : {tarifs_str}
Encadrant·e·s: 
{format_encadrants(s.encadrants_potentiels)}

// Présentation
{parse_html_and_format(s.presentation)}

// Objectifs
{parse_html_and_format(s.objectifs)}

// Programme
{parse_html_and_format(s.programme)}

// Réservation & Détails

https://www.atelierdusoleiletduvent.org/{i.route}

""")
        
def _print_iscdp_markdown(iscdp):
    i, s, c, dates, pretties = iscdp
    formatted_dates_str = format_dates_array_as_str(dates, pretties)
    tarifs_str = format_tarifs_as_str(s)

    return (f"""
[{s.titre_long}](https://www.atelierdusoleiletduvent.org/{i.route}) - {tarifs_str}
{formatted_dates_str}""")

def get_iscdp_arr(from_date = datetime.today(), to_date = datetime.today() + timedelta(days=365), only_stage_names = []):
    _all_iscdp_arr = _extract_iscdp_arr()
    
    filtered_arr = _filter_iscdp_arr_by_date(_all_iscdp_arr, from_date, to_date)
    filtered_arr = _filter_iscdp_arr_by_stages(filtered_arr, only_stage_names)

    return filtered_arr


def get_active_stages_and_dates_as_iscdp_arr(from_date = datetime.today(), to_date = datetime.today() + timedelta(days=365), only_stage_names = []):
    filtered_arr = get_iscdp_arr(from_date, to_date, only_stage_names)
    filtered_arr = _filter_iscdp_arr_only_active(filtered_arr)

    return filtered_arr


# from asv.stage.helpers import get_stage_names_to_pretty_date_dict
# get_stage_names_to_pretty_date_dict()
def get_stage_names_to_pretty_date_dict(from_date = datetime.today(), to_date = datetime.today() + timedelta(days=365), only_stage_names = []):
    filtered_arr = get_active_stages_and_dates_as_iscdp_arr(from_date, to_date, only_stage_names)
    
    out = {}
    for iscdp in filtered_arr:
        i, s, c, dates, pretties = iscdp
        custom_pretties = [ format_to_pretty_date(d, p) for (d, p) in zip(dates, pretties) ]
        out[s.titre_court] = custom_pretties
    
    return out

# from asv.stage.helpers import update_cache_dates_des_stages_a_venir
# update_cache_dates_des_stages_a_venir()
def update_cache_dates_des_stages_a_venir():
    d = get_stage_names_to_pretty_date_dict()
    j = json.dumps(d)
    frappe.db.set_value('ASV Cache Formation', 'dates_des_stages_a_venir', 'value', j)
    frappe.db.commit()

# from asv.stage.helpers import print_stages_and_dates
# print_stages_and_dates('26 Avril 2022', '30 Juillet 2022', [], 'markdown')
# print_stages_and_dates('6 Mai 2022', '30 Juillet 2022', [], 'flyer')
def print_stages_and_dates(from_date_pretty, to_date_pretty, only_stage_names, print_format):

    from_date, to_date = get_date_interval(from_date_pretty, to_date_pretty)

    filtered_arr = get_active_stages_and_dates_as_iscdp_arr(from_date, to_date, only_stage_names)
    
    for iscdp in filtered_arr:
        if print_format == 'facebook_short':
            print(_print_iscdp_facebook_short(iscdp))
        elif print_format == 'facebook_long':
            print(_print_iscdp_facebook_long(iscdp))
        elif print_format == 'markdown':
            print(_print_iscdp_markdown(iscdp))
        elif print_format == 'flyer':
            print(_print_iscdp_flyer(iscdp))
        else:
            print(f"Format non reconnu : {print_format}")
        
            
            # print(f"""
            # [{s.titre_long}](https://www.atelierdusoleiletduvent.org/{i.route}) - {tarifs_str}
            # {formatted_dates_str}  
            # """)

            # print(
            #     f"""
            #     {s.titre_long}
            #     {formatted_dates_str}""")

            # FACEBOOK / LINKEDIN
            # print(
            #     f"""
            #     👉 {s.titre_long}
            #     https://atelierdusoleiletduvent.org/{i.route}
            #     {formatted_dates_str}""")

            # # FLYER
            # print('{')
            # print(f"""
            # 'titre':"{s.titre_court}",
            # 'dates_et_details':"{formatted_dates_str}",
            # 'tarifs':'{tarifs_str}'
            # """)
            # print('},')

            # 

def update_all_articles():
    
    stages_names = frappe.db.get_list('Stage', pluck='name')
    stages = [ frappe.get_doc('Stage', n) for n in stages_names ]
    items_names = [ s.reference_article if s.reference_article else None for s in stages ]
    items = [ frappe.get_doc('Item', i) if i else None for i in items_names ]

    for item in items:
        if item:
            if item.name == 'BIEN-DEMARRER-SON-PROJET-DE-POELE-DE-MASSE':
                # update_info_with_stage_details(item)
                print(f'Manual update of : {item.name}')
                item.web_long_description = ''
                item.save()