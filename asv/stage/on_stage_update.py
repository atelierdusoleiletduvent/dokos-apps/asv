import frappe
from asv.custom.utils import rot5, rot13

def trigger_item_update(doc, method=None):
    if doc.reference_article:
        item_name = doc.reference_article
        item = frappe.get_doc('Item', item_name)
        item.save(ignore_version=True)

def set_encoded_courriel_and_tel(stage, method=None):

    frappe.db.set_value('Stage', stage.name, {
        'organisateur_courriel_enc' : rot13(stage.organisateur_courriel),
        'encadrant_courriel_enc'    : rot13(stage.encadrant_courriel),
        'organisateur_tel_enc'      : rot5(stage.organisateur_tel),
        'encadrant_tel_enc'         : rot5(stage.encadrant_tel),
    }, update_modified=False)

