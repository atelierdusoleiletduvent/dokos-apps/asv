
# See resources :
# - https://developers.google.com/search/docs/advanced/structured-data/product
# - https://schema.org/Offer
def structured_data_as_product(item, stage):
    return (f"""
    {{
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "{stage.titre_court}",
        "image": [
          "{stage.image_principale}",
         ],
        "description": "{stage.resume}",
        "brand": {{
          "@type": "Organization",
          "name": "Atelier du Soleil et du Vent",
          "address": {{
            "@type": "PostalAddress",
            "addressLocality": "Lusignan, France",
            "postalCode": "F-86600",
            "streetAddress": "57 avenue de Poitiers"
        }},
        }},
        "offers": {{
          "@type": "Offer",
          "url": "https://www.atelierdusoleiletduvent.org/{item.route}",
          "priceCurrency": "EUR",
          "price": "{stage.tarif_reservation}",
          "category": "Stage / Formation"
        }}
      }}
    """ if stage else None)