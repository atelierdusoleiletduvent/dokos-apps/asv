_rot13_trans = str.maketrans(
    'ABCDEFGHIJKLMabcdefghijklmNOPQRSTUVWXYZnopqrstuvwxyz',
    'NOPQRSTUVWXYZnopqrstuvwxyzABCDEFGHIJKLMabcdefghijklm')

_rot5_trans = str.maketrans('0123456789', '5678901234')

def rot13(value):
    return str.translate(value, _rot13_trans) if value else None
def rot5(value):
    return str.translate(value, _rot5_trans) if value else None